# Description
This is a distributed service to process data units. The distributed service consists of a part that pulls in data and does some preliminary processing before sending it to a distribution coordinator when in turn splits the data in to smaller bits to be processed and stored individually.

# Ubuntu Server Software Requirements:
* package: git-cli
* package: composer-cli 
* package: symfony-cli
* package: php-amqp  (make sure to add in php.ini: extension=amqp.so  .Then restart apache) 
* MySQL
* Rabbitmq
* PHP 7.4

# First Installment
* clone git repository in server
* run command: cd path/to/website
* run command: sudo composer install
* run command: sudo npm install (if you wish to use gulp)
* rename public/.htaccess to public/.htaccess.private if server is run with command:   symfony server:start
* run command: sudo ./bin/phpunit 
* go to .env folder and add your costum mysql data in variable: DATABASE_URL
* run command: sudo php bin/console make:migration
* run command: sudo php bin/console doctrine:migrations:migrate

# Execute test
* run command: php bin/console messenger:consume async
* open/refresh browserpage: 127.0.0.1/web
* check the result in mysql phpmyadmin