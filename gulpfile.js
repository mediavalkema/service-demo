'use strict';
 
const path = {
    js: {
        watch: 'public/js/**/*.js',
        source: 'public/js/**/*.js'
    },
    scss: {
        watch: 'public/scss/**/*.scss',
        source: 'public/scss/styles.scss',
        destFolder: 'public/css'
    } 
}

const gulp = require('gulp');
const eslint = require('gulp-eslint');
const sass = require('gulp-sass'); 
sass.compiler = require('node-sass');

// check javscript errors
// command: gulp eslint
gulp.task('eslint', () => {
    return gulp.src(path.js.source)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// check sass errors and compress scss file(s) to css file
// command: gulp sass
gulp.task('sass', () => {
    return gulp.src(path.scss.source)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(path.scss.destFolder));
});

// one time execution of above gulp tasks
// command: gulp all
gulp.task('all', gulp.parallel('sass', 'eslint'));
   
// watch for modified files and execute corresponding gulp tasks when files are modified
// command: gulp watch
gulp.task('watch', () => {
    gulp.watch(path.scss.watch, gulp.series('sass'));
    gulp.watch(path.js.watch, gulp.series('eslint'));
});