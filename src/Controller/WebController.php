<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Execute;

class WebController extends AbstractController
{
    private $test = true;
    private $execute;

    /**
     * Construct
     * 
     * @param Execute $execute
     */
    
    public function __construct(Execute $execute)
    {        
        $this->execute = $execute;
    }
    
    /**
     * Route to webinterface
     * 
     * @Route("/web", name="web_interface")
     */
    public function interface()
    {        
        $this->test ? $this->execute->testRun() : $this->execute->realRun();
        return $this->render('interface.html.twig', []);
    }
}