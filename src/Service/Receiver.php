<?php
namespace App\Service;

/**
 * Service for loading data from external sources (ftp, json, etc..) 
 */

class Receiver
{
    /**
     * General dummy data
     * 
     * @var array $info
     */
    public $info = [
        'syncDataID' => '',
        'syncBagID' => '',
        'syncItemID' => '',
        'sourceID' => ''
    ];

    /**
     * Dummy data items
     * Each item in syncItem is treated as a seperate queque
     * 
     * @var array $syncItems
     */
    public $syncItems = [];

    public function __construct()
    {
        // do your thing here
    }

    /**
     * Retrieve data from external sources
     * 
     */
    public function getData()
    {
        // get your data from here
    }
}