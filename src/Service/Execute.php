<?php
namespace App\Service;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\MessageBusInterface;

use Symfony\Bundle\FrameworkBundle\Console\Application;

use App\Tests\DummyData;
use App\Message\Queue;

class Execute
{

    private $kernel;
    private $bus;

    public function __construct(KernelInterface $kernel, MessageBusInterface $bus)
    {
        $this->kernel = $kernel;
        $this->bus = $bus;
    }

    /**
     * Execute a bash command
     * 
     * @param string $action
     * Action Options:
     *  - 'play' : consume all queque messages
     *  - 'stop' : stop currently running queque messages
     *  - 'debug': debug the queue messages
     * 
     * @return Response
     */
    public function queue(string $action = 'play'): Response
    {
        $arrayInput = [];
        switch ($action) {
            case 'play':
                $arrayInput = [
                    'command' => 'messenger:consume',
                    '--limit' => 20,
                    '-vv' => true  
                ];
                break;
            case 'stop':
                $arrayInput = [
                    'command' => 'messenger:consume',
                    '-q' => true
                ];
                break;   
            case 'debug':
                $arrayInput = [
                    'command' => 'debug:messenger'
                ];
                break;        
        }
        $application = new Application($this->kernel);  
        $application->setAutoExit(false);
        $input = new ArrayInput($arrayInput);
        $output = new BufferedOutput();
        $application->run($input, $output);
        $content = $output->fetch();

        return new Response($content);        
    }

    
    /**
     * Dispatch all Queues from dummy data
     * Also executes PHP-unit test
     */

    public function testRun()
    {              
        $this->dummy = new DummyData;
        $this->dummy->testData();
        foreach( $this->dummy->syncItems as $item ) {         
            $this->bus->dispatch(new Queue($item, $this->dummy->info), [
                new DelayStamp(500)
            ]);
        }
        
        // run bash command via php class        
        # $this->execute->queue('play');        
    }
    
    /**
     *  Dispatch all Queues from real data 
     */    
    public function realRun()
    {
        // do something
    }
}