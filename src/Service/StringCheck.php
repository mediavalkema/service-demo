<?php
namespace App\Service;

use App\Message\Queue;

class StringCheck {

    /**
     * String type: dataString0, dataString1, etc...
     * 
     * @var string $stringType
     */
    private static $stringType;

    /**
     * Value from $stringType
     * 
     * @var string $value
     */
    private static $value;
    
    /**
     * Configuration for the different string types
     * 
     * @var array $stringConfig
     */
    public static $stringConfig = [
        'dataString0' => [
            'method' => 'checkUrl',
            'grade' => [
                'missing' => 'F',
                'invalid' => 'D'
            ]
        ],
        'dataString1' => [
            'method' => 'checkDateTime',
            'grade' => [
                'missing' => 'F',
                'invalid' => 'C'
            ]
        ],
        'dataString2' => [
            'method' => 'checkOnlyPrintableCharacters',
            'grade' => [
                'missing' => 'B',
                'invalid' => 'B'
            ]
        ],
        'dataString3' => [
            'method' => 'checkPostalCode',
            'grade' => [
                'missing' => 'F',
                'invalid' => 'C'
            ]
        ],
        'dataString4' => [
            'method' => 'checkOnlyPrintableCharacters',
            'grade' => [
                'missing' => 'B',
                'invalid' => 'B'
            ]
        ],
        'dataString5' => [
            'method' => 'checkIntOrDouble',
            'grade' => [
                'missing' => 'B',
                'invalid' => 'B'
            ]
        ],
        'dataString6' => [
            'optional' => true,
            'grade' => false
        ]
    ];

    /**
     * Configuration for the different grades
     * 
     * @var array $gradeConfig
     */
    public static $gradeConfig = [
        'A' => ['as-is'],
        'B' => ['aggregate', 'as-is'],
        'C' => ['aggregate', 'as-is'],
        'D' => ['aggregate', 'not as-is'],
        'E' => ['aggregate', 'not as-is'],
        'F' => ['not as-is']
    ];

    /**
     * Builds a array map (grademap) with calculated grades for each and all datastrings.
     * Includes also 'aggregate' and 'as-is' status
     * 
     * @param Queue $message
     * @return array
     */
    public static function grader(Queue $message): array
    {
        $gradeMap = [
            'grade' => 'A', 
            'aggregate' => false, 
            'as-is' => false,   
            'currentGrades' => [],               
            'currentData' => []
        ];  
        foreach($message->item as $key => $value) {
            if(isset(self::$stringConfig[$key])) {
                $grade = self::gradePerString($key, $value);
                if(self::alphabetOrder($gradeMap['grade'], $grade)) {
                    $gradeMap['grade'] = $grade;
                };                
                $gradeMap['currentGrades'][$key] = $grade;
                $gradeMap['currentData'][$key] = $value;
            }
        }
        $masterAction = self::$gradeConfig[$gradeMap['grade']];
        $gradeMap['currentData']['status'] = 0;
        $gradeMap['currentData']['uuid'] = $message->info['syncDataID'];
        $gradeMap['currentData']['created'] = new \DateTime('now');
        $gradeMap['aggregate'] = (in_array('aggregate', $masterAction));
        $gradeMap['as-is'] = (in_array('as-is', $masterAction));
        return $gradeMap;
    }

    /**
     * Aggregates (combines) two gradeMaps together
     * 
     * @param array $gradeMap1
     * @param array $gradeMap2
     * @return array
     */
    public static function aggregate(array $gradeMap1, array $gradeMap2): array
    {
        $map = self::setMasterDonor($gradeMap1, $gradeMap2);
        $aggregatedMap = [
            'grade' => 'A',              
            'aggregate' => false,    
            'as-is' => false,  
            'currentGrades' => [],         
            'currentData' => []
        ]; 
        foreach($map['master']['currentData'] as $key => $value) {
            if(isset(self::$stringConfig[$key])) {
                $grade = $map['master']['currentGrades'][$key];
                $masterAction = self::$gradeConfig[$grade];
                
                $mergedGrades = &$aggregatedMap['currentGrades'][$key];
                $mergedValue = &$aggregatedMap['currentData'][$key];
                
                $mergedGrades = $grade;
                $mergedValue = $value;

                if(
                    !in_array('good', $masterAction) && 
                    in_array('aggregate', $masterAction) &&
                    self::alphabetOrder($map['donor']['currentGrades'][$key], $mergedGrades)
                ) {
                    $mergedGrades = $map['donor']['currentGrades'][$key];
                    $mergedValue = $map['donor']['currentData'][$key];
                }
                if(self::alphabetOrder($aggregatedMap['grade'], $mergedGrades)) {
                    $aggregatedMap['grade'] = $mergedGrades;
                }; 
            }
        }
        $masterAction = self::$gradeConfig[$aggregatedMap['grade']];
        $aggregatedMap['currentData']['status'] = 0;
        $aggregatedMap['currentData']['uuid'] = $map['master']['currentData']['uuid'];
        $aggregatedMap['currentData']['created'] = new \DateTime('now');
        $aggregatedMap['as-is'] = (in_array('as-is', $masterAction)); 
        return $aggregatedMap;
    }
    
    /**
     * Calculates which grademap is Master and which is donor
     * 
     * @param array $gradeMap1
     * @param array $gradeMap2
     * @return array
     */
    private static function setMasterDonor(array $gradeMap1, array $gradeMap2): array
    {
        if(self::alphabetOrder($gradeMap1['grade'], $gradeMap2['grade'])) {
            return [
                'master' => $gradeMap1,
                'donor' => $gradeMap2
            ];
        } else {
            return [
                'master' => $gradeMap2,
                'donor' => $gradeMap1
            ];
        }
    }

    /**
     * Checks which letter is first in alphabet
     * Is true if first letter (first parameter) is earlier in alphabet then second letter (second parameter)
     * 
     * @param string $firstLetter
     * @param string $secondLetter
     * @return bool 
     */
    private static function alphabetOrder(string $firstLetter, string $secondLetter) : bool
    {
        $alphabet = range('A', 'F');
        return (array_search($firstLetter, $alphabet) < array_search($secondLetter, $alphabet));
    }
 
    /**
     * Calculates the grade for each dataString and returns the grade.
     * 
     * @param string $stringType (for example: dataString0, dataString1, etc...)
     * @param string $value (value of the stringType)
     * @return string
     */
    private static function gradePerString(string $stringType, string $value): string
    {        
        self::$value = $value;
        self::$stringType = $stringType;        
        if(empty($value)) {
            return (isset(self::$stringConfig[$stringType]['grade']['missing'])) ?
                self::$stringConfig[$stringType]['grade']['missing'] :
                self::checkOptional();            
        } elseif(
            isset(self::$stringConfig[$stringType]['method']) &&
            !call_user_func([self::class, self::$stringConfig[$stringType]['method']])
        ) {
            return isset(self::$stringConfig[$stringType]['grade']['invalid']) ?
                self::$stringConfig[$stringType]['grade']['invalid'] :  
                self::checkOptional();
        }
        return 'A';
    }

    /**
     * Calculates Grade when value of stringType is optional
     * 
     * @return string
     */
    private static function checkOptional()
    {
        if(isset(self::$stringConfig[self::$stringType]['optional']) && self::$stringConfig[self::$stringType]['optional']) {
            return 'A';
        } else {
            return 'F';
        }
    }

    /**
     * Determines if value of stringType is a valid url
     * 
     * @return bool
     */
    private static function checkUrl(): bool
    {
        return filter_var(self::$value, FILTER_VALIDATE_URL);
    }

    /**
     * Determines if value of stringType has a valid DateTime format.
     * 
     * @return bool
     */
    private static function checkDateTime(): bool
    {
        return (\DateTime::createFromFormat('c', self::$value) !== false);
    }

    /**
     * Determines if value of stringType has only printable characters
     * 
     * @return bool
     */
    private static function checkOnlyPrintableCharacters():  bool
    {
        return ctype_print(self::$value);
    }

    /**
     * Determines if value of stringType has a valid postal code
     * 
     * @return bool
     */
    private static function checkPostalCode(): bool
    {
        return preg_match("/^\W*[1-9]{1}[0-9]{3}\W*[a-zA-Z]{2}\W*$/",  self::$value);
    }

    /**
     * Determines if value of stringType is integer or double without zero
     * 
     * @return bool
     */
    private static function checkIntOrDouble(): bool
    {
        return ((is_float(self::$value) || is_int(self::$value)) && preg_match("/^[1-9]*$/", self::$value));
    }
}