<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200615114020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sync_bag (id INT AUTO_INCREMENT NOT NULL, source CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', syncitem INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sync_data (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, created DATETIME NOT NULL, data_string0 VARCHAR(255) DEFAULT NULL, data_string1 VARCHAR(255) DEFAULT NULL, data_string2 VARCHAR(255) DEFAULT NULL, data_string3 VARCHAR(255) DEFAULT NULL, data_string4 VARCHAR(255) DEFAULT NULL, data_string5 VARCHAR(255) DEFAULT NULL, data_string6 VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sync_item (id INT AUTO_INCREMENT NOT NULL, syncbag INT NOT NULL, grade VARCHAR(1) NOT NULL, currentdata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', historydata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data CHANGE id id VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sync_bag');
        $this->addSql('DROP TABLE sync_data');
        $this->addSql('DROP TABLE sync_item');
        $this->addSql('ALTER TABLE data CHANGE id id VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
