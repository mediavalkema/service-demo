<?php
namespace App\Message;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Repository\DataRepository;
use App\Service\StringCheck;


/**
 * tests:
 * php bin/console debug:messenger
 * php bin/console messenger:consume async -vv
 */

class QueueHandler implements MessageHandlerInterface
{
    private $dataRepository;
    private static $arr = [];
    
    public function __construct(DataRepository $dataRepository)
    {
        $this->dataRepository = $dataRepository;    
    }

    /**
     * Default array format for syncbag
     * 
     * @param Queue $message
     * @return array
     */
    private function initSyncBag(Queue $message): array
    {
        return [
            'id' => $message->info['syncBagID'],
            'source' => $message->info['sourceID'],
            'syncItems' => []
        ];
    }

    /**
     * Default array format for syncitem
     * 
     * @param Queue $message
     * @return array
     */
    private function initSyncItem(Queue $message): array
    {
        return [
            'id' => $message->info['syncItemID'],
            'syncBag' => $message->info['syncBagID'],
            'grade' => '',
            'currentData' => [],
            'historyData' => []
        ];
    }

    /**
     * Array builder
     * 
     * @param Queue $message
     */
    private function arrayBuilder(Queue $message)
    {
        // make syncBag array
        if(empty(self::$arr)) self::$arr = $this->initSyncBag($message);

        // make syncItem array if syncItem does not exist
        $found = false;
        $index = 0;
        foreach(self::$arr['syncItems'] as $in => $item) {
            if($item['id'] == $message->info['syncItemID']) {
                $found = true;
                $index = $in;
                break;
            }
        }
        if(!$found) {
            $index = count(self::$arr['syncItems']);
            self::$arr['syncItems'][] = $this->initSyncItem($message);            
        }
        
        // move old syncData to historyData
        $old = self::$arr['syncItems'][$index]['currentData'];
        if(!empty($old)) {
            $old['status'] = -1;
            self::$arr['syncItems'][$index]['historyData'][] = $old;
        }

        // add new syncData to currentData
        $new = $message->item;
        $new['status'] = 0;
        self::$arr['syncItems'][$index]['currentData'] = $new;
    }
    
    /**
     * Manager unit for Queue Messages
     */
    public function __invoke(Queue $message)
    {        
        $this->arrayBuilder($message);
        $this->controller($message, StringCheck::grader($message));
    }

    /**
     * Support unit for __invoke method
     * All array modifications (like aggregation, deletion and updates) are deligated from here
     * 
     * @param Queue $message
     * @param array $gradeMap
     */
    private function controller(Queue $message, array $gradeMap)
    {        
        if(
            $gradeMap['aggregate'] &&
            $this->dataRepository->hasUUID($message->info['syncDataID'])
        ) {
            $DBdata = $this->dataRepository->getFieldFromUUID($message->info['syncDataID'])[0]; 
            $DBqueque = new Queue($DBdata, $message->info);
            $aggregatedMap = StringCheck::aggregate(StringCheck::grader($DBqueque), $gradeMap);
            $this->controller($message, $aggregatedMap);
        } elseif($gradeMap['as-is']) {
            $this->dataRepository->updateDataField($gradeMap['currentData'], $gradeMap['currentData']['uuid']);            
        } 
    }

    /**
     * A MessageHandlerInterface configuration function  
     */
    public static function getHandledMessages(): iterable
    {      
        yield Queue::class;
    }
}