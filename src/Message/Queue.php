<?php

namespace App\Message;

class Queue
{
    public $item = [];
    public $info = [];    

    /**
     * Message Queue for json data
     * 
     * @param array $item
     * @param array $info
     */
    public function __construct(array $item, array $info)
    {
        $this->item = $item;
        $this->info = $info;
    }
}