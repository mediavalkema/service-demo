<?php

namespace App\Repository;

use App\Entity\Data;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Data|null find($id, $lockMode = null, $lockVersion = null)
 * @method Data|null findOneBy(array $criteria, array $orderBy = null)
 * @method Data[]    findAll()
 * @method Data[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataRepository extends ServiceEntityRepository
{
    
    public $em;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Data::class);   
    }

    /**
     * Check if database already has $uuid 
     * 
     * @param string $id
     * @return bool
     */
    public function hasUUID(string $uuid): bool
    {
        $results = $this->createQueryBuilder('d')
            ->andWhere('d.uuid = :val')
            ->setParameter('val', $uuid)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        return (bool)count($results);
    }

    /**
     * Get field from given $uuid
     * 
     * @param string $uuid
     * @return array
     */
    public function getFieldFromUUID(string $uuid): array
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.uuid = :val')
            ->setParameter('val', $uuid)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Updates a Data field with given $uuid value
     * Creates new field if field does not have the $uuid value.
     * 
     * @param array $arr : database array
     * @param $uuidValue : value of $uuid
     */
    public function updateDataField(array $arr, $uuidValue)
    {
        $em = $this->_em->create(
            $this->_em->getConnection(),
            $this->_em->getConfiguration()
        );
        $product = $em->getRepository(Data::class)->findOneBy(['uuid' => $uuidValue]);
        if (!$product) {
            $this->createDatafield($arr);
        } else { 
            $this->createQueryBuilder('d')  
                ->update()
                ->set('d.status', '?1')
                ->setParameter(1, $arr['status']) 
                ->set('d.created', '?2')
                ->setParameter(2, $arr['created'])
                ->set('d.dataString0', '?3')
                ->setParameter(3, $arr['dataString0'])
                ->set('d.dataString1', '?4')
                ->setParameter(4, $arr['dataString1'])
                ->set('d.dataString2', '?5')
                ->setParameter(5, $arr['dataString2'])
                ->set('d.dataString3', '?6')
                ->setParameter(6, $arr['dataString3'])
                ->set('d.dataString4', '?7')
                ->setParameter(7, $arr['dataString4'])
                ->set('d.dataString5', '?8')
                ->setParameter(8, $arr['dataString5'])
                ->set('d.dataString6', '?9')
                ->setParameter(9, $arr['dataString6'])        
                ->andWhere('d.uuid = :val')
                ->setParameter('val', $uuidValue)
                ->getQuery()
                ->execute();          
        }
    }

    /**
     * Create Data field
     * 
     * @param array $arr : database array
     */
    private function createDatafield(array $arr)
    {
        $em = $this->_em->create(
            $this->_em->getConnection(),
            $this->_em->getConfiguration()
        );        
        $data = new Data;
        $data->setUuid($arr['uuid']);
        $data->setStatus($arr['status']);
        $data->setCreated($arr['created']);
        if(isset($arr['dataString0'])) $data->setDataString0($arr['dataString0']);
        if(isset($arr['dataString1'])) $data->setDataString1($arr['dataString1']);
        if(isset($arr['dataString2'])) $data->setDataString2($arr['dataString2']);
        if(isset($arr['dataString3'])) $data->setDataString3($arr['dataString3']);
        if(isset($arr['dataString4'])) $data->setDataString4($arr['dataString4']);
        if(isset($arr['dataString5'])) $data->setDataString5($arr['dataString5']);
        if(isset($arr['dataString6'])) $data->setDataString6($arr['dataString6']);
        $em->persist($data);
        $em->flush();
    }
}
