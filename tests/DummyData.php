<?php
namespace App\Tests;

use App\Service\Receiver;
use PHPUnit\Framework\TestCase;

class DummyData extends TestCase
{
    
    /**
     * General dummy data
     * 
     * @var array $info
     */
    public $info = [
        'syncDataID' => 'A4EC2DE6-48AF-42A4-BE97-6B2C04F61ACC',
        'syncBagID' => 'A4EC2DE6-48AF-42A4-BE97-6B2C04F61BSD',
        'syncItemID' => '96D743D9-E838-4BCF-BF26-1489F993873E',
        'sourceID' => '96D743D9-E838-4BCF-BF26-1489FE33873E'
    ];

    /**
     * Dummy data items
     * Each item in syncItem is treated as a seperate queque
     * 
     * @var array $syncItems
     */
    public $syncItems = [];
    
    /**
     * Load syncItems via construct
     */
    public function __construct()
    {        
        $this->syncItems = json_decode(file_get_contents("../tests/DummyData.json"), true);
    }
    
    /** 
     * Run test from dummy data source
     * 
     * @test
     */
    public function testData()
    {        
        $stub = $this->createMock(Receiver::class);
        $stub->method('getData')->willReturn($this->syncItems);
        $this->assertSame($this->syncItems, $stub->getData());
        // add other tests if needed            
    }
}